import os
from datetime import datetime
from pathlib import Path
from time import sleep

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import close_all_sessions, sessionmaker
from sqlalchemy.pool import NullPool

from app.database import Base, get_db
from app.models import author, book  # noqa
from main import app

# Setup a test database URL, which should be different from your production database
TEST_DATABASE_URL = "sqlite:///./test.db"

BASE_DIR = Path(__file__).resolve().parent
TEST_DATABASE_PATH = BASE_DIR / "test.db"

# Create a new engine instance for testing
test_engine = create_engine(
    f"sqlite:///{TEST_DATABASE_PATH}", connect_args={"check_same_thread": False}, poolclass=NullPool
)

# Create a new SessionLocal class for testing
TestSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=test_engine)


@pytest.fixture(scope="module")
def test_app():
    """Create the database and tables just for tests"""
    Base.metadata.create_all(bind=test_engine)
    _app = app
    yield _app
    # Drop the test database tables after the tests are done
    Base.metadata.drop_all(bind=test_engine)
    try:
        close_all_sessions()
        test_engine.dispose()
        # Remove the test database file if it exists
        if os.path.exists(TEST_DATABASE_PATH):
            sleep(0.5)
            os.unlink(TEST_DATABASE_PATH)
    except FileNotFoundError:
        print("cant delete")
        pass  # noqa


@pytest.fixture(scope="function")
def db_session():
    """Create a new database session for a test."""
    # Start a new transaction for testing
    connection = test_engine.connect()
    transaction = connection.begin()
    session = TestSessionLocal(bind=connection)
    yield session
    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture(scope="module")
def client(test_app):
    """Create a test client using the test application fixture."""
    with TestClient(test_app) as c:
        yield c


@pytest.fixture(scope="function")
def override_get_db(db_session):
    """Override the get_db dependency to use the testing session"""

    def _override_get_db():
        try:
            yield db_session
        finally:
            db_session.close()

    app.dependency_overrides[get_db] = _override_get_db


@pytest.fixture(scope="function")
def author_fixture(db_session):
    new_author = author.Author(name="Test Author", biography="Test Biography")
    db_session.add(new_author)
    db_session.commit()
    yield new_author
    db_session.delete(new_author)
    db_session.commit()


@pytest.fixture(scope="function")
def book_fixture(db_session, author_fixture):
    new_book = book.Book(
        title="New Book",
        author_id=author_fixture.id,
        genre="test genre",
        published_date=datetime.strptime("2010-05-25", "%Y-%m-%d").date(),
        isbn="978-1-45678-123-4",
        quantity_available=5,
    )
    db_session.add(new_book)
    db_session.commit()
    yield new_book
    db_session.delete(new_book)
    db_session.commit()
