from app.models import Author


def test_get_authors(client, db_session, override_get_db):
    response = client.get("/authors/")
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_get_author_invalid_id(client, db_session, override_get_db):
    response = client.get("/authors/99999")
    assert response.status_code == 404


def test_create_author(client, db_session, override_get_db):
    new_author_data = {"name": "New Author", "biography": "New biography"}
    response = client.post("/authors/", json=new_author_data)

    assert response.status_code == 201
    response_data = response.json()
    assert response_data["name"] == new_author_data["name"]

    # Query the database to confirm the author was added
    created_author = db_session.query(Author).filter(Author.id == response_data["id"]).first()

    assert created_author is not None
    assert created_author.name == new_author_data["name"]
    assert created_author.biography == new_author_data["biography"]


def test_create_author_invalid_data(client, db_session, override_get_db):
    new_author = {}
    response = client.post("/authors/", json=new_author)
    assert response.status_code == 422


def test_get_authors_with_pagination(client, db_session, override_get_db):
    response = client.get("/authors/?skip=0&limit=2")
    assert response.status_code == 200
    assert len(response.json()) <= 2
