from app.models import Book


def test_get_books(client, db_session, override_get_db):
    response = client.get("/books/")
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_get_book_invalid_id(client, db_session, override_get_db):
    response = client.get("/books/99999")
    assert response.status_code == 404


def test_create_book_invalid_data(client, db_session, override_get_db):
    new_book = {}
    response = client.post("/books/", json=new_book)
    assert response.status_code == 422


def test_create_book(client, db_session, override_get_db, author_fixture):
    new_book = {
        "title": "New Book",
        "author_id": author_fixture.id,
        "genre": "test genre",
        "published_date": "2010-05-25",
        "isbn": "978-1-45678-123-4",
        "quantity_available": 5,
    }
    response = client.post("/books/", json=new_book)
    assert response.status_code == 201
    response_data = response.json()

    # Query the database to confirm the book was added
    created_book = db_session.query(Book).filter(Book.id == response_data["id"]).first()

    assert created_book is not None
    assert created_book.title == new_book["title"]
    assert created_book.author_id == 1
    assert created_book.isbn == new_book["isbn"]


def test_update_book(client, db_session, override_get_db, book_fixture):
    updated_book = {
        "title": "New Book2",
        "author_id": 1,
        "genre": "test genre",
        "published_date": "2010-05-25",
        "isbn": "978-1-45678-123-4",
        "quantity_available": 5,
    }
    response = client.put("/books/1", json=updated_book)
    assert response.status_code == 200
    response_data = response.json()

    # Query the database to confirm the book was added
    created_book = db_session.query(Book).filter(Book.id == response_data["id"]).first()

    assert created_book is not None
    assert created_book.title == updated_book["title"]


def test_update_nonexistent_book(client, db_session, override_get_db):
    updated_book = {
        "title": "New Book2",
        "author_id": 1,
        "genre": "test genre",
        "published_date": "2010-05-25",
        "isbn": "978-1-45678-123-4",
        "quantity_available": 5,
    }
    response = client.put("/books/99999", json=updated_book)
    assert response.status_code == 404


def test_delete_book(client, db_session, override_get_db, book_fixture):
    # Replace with a valid book_id that can be deleted
    response = client.delete("/books/1")
    assert response.status_code == 204


def test_delete_nonexistent_book(client, db_session, override_get_db):
    response = client.delete("/books/99999")
    assert response.status_code == 404
