from fastapi import FastAPI

from app.routers import authors, books

app = FastAPI(
    title="Library",
    description="Library's book inventory system",
    version="0.0.1",
    contact={
        "name": "Mohammad Reza Akbari",
        "email": "mra1373@gmail.com",
    },
    license_info={
        "name": "MIT",
        "url": "https://gitlab.com/mra-ak/library-api/-/blob/main/LICENSE?ref_type=heads",
    },
)

app.include_router(authors.router)
app.include_router(books.router)
