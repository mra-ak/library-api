import re
from datetime import date

from pydantic import BaseModel, Field, validator


class BookBase(BaseModel):
    """Base schema for book model, defining common attributes."""

    title: str = Field(..., description="Book Title")
    author_id: int = Field(..., description="Author ID")
    genre: str = Field(..., description="Book genre")
    published_date: date = Field(..., description="Book published date. e.g. 2024-01-25")
    isbn: str = Field(
        ...,
        description="ISBN number. Valid values example: \
            https://www.geeksforgeeks.org/regular-expressions-to-validate-isbn-code/",
    )
    quantity_available: int = Field(..., description="Book quantity available")

    @validator("isbn")
    def validate_isbn(cls, value):
        # ISBN regex validator is based on https://www.geeksforgeeks.org/regular-expressions-to-validate-isbn-code/
        if not re.match(r"^(?=(?:[^\d]*\d){10}(?:(?:[^\d]*\d){3})?$)[\d-]+$", value):
            raise ValueError("Invalid ISBN format")
        return value

    @validator("quantity_available")
    def validate_quantity(cls, value):
        """
        Validates that the quantity available is not negative.
        """
        if value < 0:
            raise ValueError("quantity_available cannot be negative")
        return value


class BookCreate(BookBase):
    """
    Schema for creating a new book.
    Inherits all fields from BookBase.
    """

    pass


class BookUpdate(BookBase):
    """
    Schema for updating an existing book.
    Inherits all fields from BookBase.
    """

    pass


class Book(BookBase):
    """
    Schema for representing a book entity with an ID.
    Inherits all fields from BookBase and adds an ID field.
    """

    id: int = Field(..., description="Book ID")

    class Config:
        from_attributes = True
