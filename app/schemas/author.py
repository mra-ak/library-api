from typing import List, Optional

from pydantic import BaseModel, Field

from .book import Book


class AuthorBase(BaseModel):
    """Base schema for author model, defining common attributes."""

    name: str = Field(..., description="Author name")
    biography: Optional[str] = Field(None, description="Author biography.")


class AuthorCreate(AuthorBase):
    """
    Schema for creating a new author.
    Inherits all fields from AuthorBase.
    """

    pass


class AuthorUpdate(AuthorBase):
    """
    Schema for updating an existing author.
    Inherits all fields from AuthorBase.
    """

    pass


class Author(AuthorBase):
    """
    Schema for representing a author entity with an ID.
    Inherits all fields from AuthorBase and adds id and books fields.
    """

    id: int = Field(..., description="Author ID")
    books: List[Book] = Field([], description="List of author's books.")

    class Config:
        from_attributes = True
