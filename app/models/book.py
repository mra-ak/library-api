from sqlalchemy import Column, Date, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.database import Base


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True, nullable=False)
    author_id = Column(Integer, ForeignKey("authors.id", ondelete="CASCADE"))
    genre = Column(String, nullable=False)
    published_date = Column(Date, nullable=False)
    isbn = Column(String, nullable=False, unique=True)
    quantity_available = Column(Integer, nullable=False)

    author = relationship("Author", back_populates="books")
