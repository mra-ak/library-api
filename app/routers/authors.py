from fastapi import APIRouter, Depends, HTTPException, Query, status
from sqlalchemy.orm import Session

from .. import models, schemas
from ..database import get_db

router = APIRouter(prefix="/authors", tags=["Authors"])


@router.get("/", response_model=list[schemas.Author], summary="Retrieve a list of Authors")
def read_authors(
    skip: int = Query(0, description="Number of records to skip from the start."),
    limit: int = Query(10, description="Maximum number of records to return."),
    db: Session = Depends(get_db),
):
    """Retrieve a list of authors with optional pagination."""
    authors = db.query(models.Author).offset(skip).limit(limit).all()
    return authors


@router.get("/{author_id}", response_model=schemas.Author, summary="Retrieve a single Author")
def read_author(author_id: int, db: Session = Depends(get_db)):
    """Retrieve a single author by ID."""
    author = db.query(models.Author).filter(models.Author.id == author_id).first()
    if author is None:
        raise HTTPException(status_code=404, detail="Author not found")
    return author


@router.post("/", response_model=schemas.Author, status_code=status.HTTP_201_CREATED)
def create_author(author: schemas.AuthorCreate, db: Session = Depends(get_db)):
    """Create a new author record."""
    new_author = models.Author(**author.dict())
    db.add(new_author)
    db.commit()
    db.refresh(new_author)
    return new_author
