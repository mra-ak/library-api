from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Query, status
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from .. import models, schemas
from ..database import get_db

router = APIRouter(prefix="/books", tags=["Books"])


@router.get("/", response_model=list[schemas.Book], summary="Retrieve a list of Books")
def read_books(
    skip: int = Query(0, description="Number of records to skip from the start."),
    limit: int = Query(10, description="Maximum number of records to return."),
    genre: Optional[str] = Query(None, description="Filter books by genre. If None, no genre filter is applied."),
    author_id: Optional[int] = Query(
        None, description="Filter books by author ID. If None, no author filter is applied."
    ),
    sort_by: Optional[str] = Query(
        None,
        description="Sort the results by published date. Accepts 'asc' for ascending or 'desc' \
                     for descending order. If None, no specific sorting is applied.",
    ),
    db: Session = Depends(get_db),
):
    """
    Retrieve a list of books with optional filtering and sorting.
    Allows for filtering by genre and author, and supports sorting by published date.
    """
    query = db.query(models.Book)

    # Filter by genre if provided
    if genre:
        query = query.filter(models.Book.genre == genre)

    # Filter by author_id if provided
    if author_id:
        query = query.filter(models.Book.author_id == author_id)

    # Sort by published_date
    if sort_by:
        if sort_by.lower() == "desc":
            query = query.order_by(models.Book.published_date.desc())
        else:
            query = query.order_by(models.Book.published_date)

    books = query.offset(skip).limit(limit).all()
    return books


@router.get("/{book_id}", response_model=schemas.Book, summary="Retrieve a single Book")
def read_book(book_id: int, db: Session = Depends(get_db)):
    """Retrieve a single book by ID."""
    book = db.query(models.Book).filter(models.Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    return book


@router.post("/", response_model=schemas.Book, status_code=status.HTTP_201_CREATED)
def create_book(book: schemas.BookCreate, db: Session = Depends(get_db)):
    """Create a new book record."""
    # Check if the author exists
    author_exists = db.query(models.Author).filter(models.Author.id == book.author_id).first()
    if not author_exists:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Author not found.")

    # Check for existing book with the same ISBN
    existing_book = db.query(models.Book).filter(models.Book.isbn == book.isbn).first()
    if existing_book:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="A book with this ISBN already exists.")

    new_book = models.Book(**book.dict())
    try:
        db.add(new_book)
        db.commit()
    except IntegrityError:
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="An error occurred while creating the book."
        )

    db.refresh(new_book)
    return new_book


@router.put("/{book_id}", response_model=schemas.Book)
def update_book(book_id: int, updated_book: schemas.BookUpdate, db: Session = Depends(get_db)):
    """Update an existing book."""
    db_book = db.query(models.Book).filter(models.Book.id == book_id).first()
    if db_book is None:
        raise HTTPException(status_code=404, detail="Book not found")

    # Check if the author exists
    author_exists = db.query(models.Author).filter(models.Author.id == updated_book.author_id).first()
    if not author_exists:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Author not found.")

    # Check if the updated ISBN is unique (if it has been changed)
    if "isbn" in updated_book.dict() and updated_book.isbn != db_book.isbn:
        existing_book_with_isbn = db.query(models.Book).filter(models.Book.isbn == updated_book.isbn).first()
        if existing_book_with_isbn:
            raise HTTPException(status_code=400, detail="Another book with this ISBN already exists.")

    # Update book details
    for key, value in updated_book.dict().items():
        setattr(db_book, key, value)

    try:
        db.commit()
    except IntegrityError:
        db.rollback()
        raise HTTPException(status_code=400, detail="An error occurred while updating the book")

    db.refresh(db_book)
    return db_book


@router.delete("/{book_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_book(book_id: int, db: Session = Depends(get_db)):
    """Delete a book from the database."""
    book = db.query(models.Book).filter(models.Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    db.delete(book)
    db.commit()
    return {"detail": "Book deleted"}
