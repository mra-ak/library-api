# 📚 Library API

## Overview
Welcome to the Library Management API project. This is a solution for managing a library's book inventory. Built with FastAPI and integrated with PostgreSQL using SQLAlchemy ORM, this API provides a robust platform for book and author management.

## Features
- **Authors Management**: Manage author details, including biographies and related books.
- **Books Management**: Add, retrieve, update, and delete books.
- **Search and Filter**: Advanced search capabilities for filtering books by genre or author.
- **Sort Books**: Sort books by published date in ascending or descending order.
- **Data Validation**: Ensures data integrity with Pydantic models.
- **Automatic Documentation**: Swagger UI and ReDoc for easy API usage.

## Getting Started
### Prerequisites
- Python 3.8+ (Python3.10.4 recommended)
- PostgreSQL

### Installation
1. **Clone the repository:**
   ```bash
   # If using HTTPS:
   git clone https://gitlab.com/mra-ak/library-api.git
   # If using SSH:
   # git clone git@gitlab.com:mra-ak/library-api.git
   cd library-api
   ```

2. **[Optional] Set up a python virtual environment:**
   ```bash
   python -m venv .venv
   source .venv/bin/activate  # On Windows use `.venv\Scripts\activate`
   ```

3. **Install dependencies:**
   ```bash
   pip install -r requirements.txt
   ```

4. **Create PostgreSQL database:**
   Create a new database.

5. **Set up environment variables:**
   Create a `.env` file in the root directory and add the following:
   ```
   DATABASE_URL=postgresql://db_user:db_password@localhost/db_name
   ```
   **Noete**: You can use `.env.example` as a sample.

6. **Apply migrations (Create tables):**
   ```bash
   alembic upgrade head
   ```

7. **Run FastAPI:**
   ```bash
   uvicorn main:app --reload
   ```

## Usage
Access the API documentation at [http://localhost:8000/docs](http://localhost:8000/docs) for Swagger UI or [http://localhost:8000/redoc](http://localhost:8000/redoc) for ReDoc.

## API Endpoints
### Books
|     Endpoint     |   Method  |   Description   |
|      ------      |   ------  |      ------     |
|/books            |    GET    |Retrieve a list of books with optional filtering and sorting.|
|/books/{book_id}  |    GET    |Retrieve a single book by ID.                                |
|/books            |    POST   |Create a new book record.                                    |
|/books/{book_id}  |    PUT    |Update an existing book.                                     |
|/books/{book_id}  |   DELETE  |Delete a book from the database.                             |


### Authors
|       Endpoint       |   Method  |   Description   |
|        ------        |   ------  |      ------     |
|/authors              |    GET    |Retrieve a list of authors with optional pagination.|
|/authors/{author_id}  |    GET    |Retrieve a single book by ID.                       |
|/authors              |    POST   |Create a new author record.                         |

## Database ERD
<img src="readme_files/sql_erd.jpg" alt="SQL ERD" width="600"/>

## Run Tests
   ```bash
   pytest .
   ```
<img src="readme_files/pytest_result.jpg" alt="pytest" width="1000"/>

## GitLab CI Pipeline
<p>Check code quality, lint, and tests automatically by GitLab pipeline.</p>
<img src="readme_files/ci_pipeline.jpg" alt="pytest" width="600" style="display: block"/>

## Improvements and Next Steps
1. **Dockerize Project (If needed):** Containerize the application using Docker to simplify deployment and ensure consistency across different environments.
1. **More Tests:** Expand the test suite to include more comprehensive unit tests, integration tests, and end-to-end tests.
1. **User Management System:** Implement user management system with features such as registration, login, authentication, and authorization.
1. **PATCH Method for Partial Updates:** Implement the PATCH method to allow partial updates.
1. **Rate Limiting:** Add rate limit on critical endpoints to prevent abuse and ensure the API's stability and reliability, especially under high load.
1. **Logging System:** Implement a logging system to capture different levels of logs (info, warning, error, etc.). This helps in monitoring, debugging, and ensures traceability of actions and errors.
1. **API Versioning:** Implement versioning for the API to manage changes and ensure backward compatibility.
1. **Changelog:** Add a CHANGELOG to track all project changes.
1. **CI/CD Enhancements:** Improve the CI/CD pipeline for automated testing, building, and deployment processes. Consider adding stages for code quality checks, security scans, and automated deployment to staging/production environments.

## Contact
Mohammad Reza Akbari - mra1373@gmail.com

Project Link: [https://gitlab.com/mra-ak/library-api](https://gitlab.com/mra-ak/library-api)